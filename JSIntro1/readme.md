# JSIntro I

Welcome to the exercise files for Javascript Introduction I. There are four files in this module: practice.js, dataType.js, logic.js and function.js. We will go through each one in the workshop to help you get more comfortable with using JS. The practice file is used to demontrate JS writing style, syntax, grammer, debugging and other best practices. The dataType, logic and functions files contain fun exercises that we will go over in the workshop. 

Psst: the solutions to each exercise can be found in the 'solution' branch. Enjoy!

Link to Slides: https://docs.google.com/presentation/d/1YMmeDbKFXKFKnWAPBiPpgoOsO8jJBHnxTD_b8pLgv30/edit?usp=sharing